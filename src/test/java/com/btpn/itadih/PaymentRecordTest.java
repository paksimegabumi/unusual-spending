package com.btpn.itadih;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

class PaymentRecordTest {
    @Test
    void groupByCategory_shouldGroupPaymentsByCategory_whenGivenMonthIsJanuary() {
        Payment paymentOne = Payment.builder().category(Category.SELF_CARE).description("Beli 1").price(10000).month(Month.JANUARY).build();
        Payment paymentTwo = Payment.builder().category(Category.EDUCATION).description("Beli 2").price(10000).month(Month.JANUARY).build();
        Payment paymentThree = Payment.builder().category(Category.EDUCATION).description("Beli 3").month(Month.JANUARY).price(10000).build();
        List<Payment> payments = Arrays.asList(paymentOne, paymentTwo, paymentThree);
        PaymentRecord record = new PaymentRecord(payments);
        Map<Category, Double> expectedResult = new HashMap<>();
        expectedResult.put(Category.SELF_CARE, 10000.0);
        expectedResult.put(Category.EDUCATION, 20000.0);

        Map<Category, Double> actualResult = record.groupByCategory(Month.JANUARY);

        Assertions.assertEquals(expectedResult, actualResult);
    }
}

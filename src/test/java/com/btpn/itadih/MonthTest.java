package com.btpn.itadih;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MonthTest {
    @Test
    void previous_shouldReturnJune_whenCurrentMonthIsJuly() {
        Month currentMonth = Month.JULY;
        Month expectedResult = Month.JUNE;

        Month actualResult = currentMonth.previous();

        Assertions.assertEquals(expectedResult, actualResult);
    }
}

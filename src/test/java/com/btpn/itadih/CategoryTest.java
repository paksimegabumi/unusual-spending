package com.btpn.itadih;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CategoryTest {
    @Test
    void toString_shouldReturnCorrectStringFormat_whenInvoked() {
        Category category = Category.SELF_CARE;
        String expectedResult = "Self Care";

        String actualResult = category.toString();

        Assertions.assertEquals(expectedResult, actualResult);
    }
}

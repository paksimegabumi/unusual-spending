package com.btpn.itadih;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SpendingAnalyzerTest {
    @Test
    void analyze_shouldReturnCorrectStringFormat_whenInvoked() {
        Payment paymentOne = Payment.builder().category(Category.SELF_CARE).description("Beli 1").price(10000).month(Month.JANUARY).build();
        Payment paymentTwo = Payment.builder().category(Category.EDUCATION).description("Beli 2").price(10000).month(Month.JANUARY).build();
        Payment paymentThree = Payment.builder().category(Category.EDUCATION).description("Beli 3").month(Month.FEBRUARY).price(20000).build();
        List<Payment> payments = Arrays.asList(paymentOne, paymentTwo, paymentThree);
        PaymentRecord record = new PaymentRecord(payments);
        SpendingAnalyzer spendingAnalyzer = new SpendingAnalyzer(record);
        String expectedResult = "Hello card user!\n\nWe have detected unusually high spending on your card in these categories:\n* You spent Rp 20.000 on Education\n\nLove, \nThe Credit Card Company";

        spendingAnalyzer.analyze(Month.FEBRUARY);

        Assertions.assertEquals(expectedResult, spendingAnalyzer.toString());
    }

    @Test
    void toString_shouldReturnCorrectStringFormat_whenInvoked() {
        Payment paymentOne = Payment.builder().category(Category.EDUCATION).description("Beli 3").month(Month.FEBRUARY).price(20000).build();
        List<Payment> payments = List.of(paymentOne);
        PaymentRecord record = new PaymentRecord(payments);
        SpendingAnalyzer spendingAnalyzer = new SpendingAnalyzer(record);
        spendingAnalyzer.analyze(Month.FEBRUARY);
        String expectedResult = "Hello card user!\n\nWe have detected unusually high spending on your card in these categories:\n* You spent Rp 20.000 on Education\n\nLove, \nThe Credit Card Company";

        Assertions.assertEquals(expectedResult, spendingAnalyzer.toString());
    }
}

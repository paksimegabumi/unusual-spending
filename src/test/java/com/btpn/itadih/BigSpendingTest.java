package com.btpn.itadih;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BigSpendingTest {
    @Test
    void toString_shouldReturnCorrectStringFormat_whenInvoked() {
        BigSpending bigSpending = BigSpending.builder().totalPrice(1000.0).category(Category.SELF_CARE).build();
        String expectedResult = "* You spent Rp 1.000 on Self Care\n";

        String actualResult = bigSpending.toString();

        Assertions.assertEquals(expectedResult, actualResult);
    }
}

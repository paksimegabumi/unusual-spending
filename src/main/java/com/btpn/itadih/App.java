package com.btpn.itadih;

import java.util.Arrays;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        Payment paymentOne = Payment.builder().category(Category.SELF_CARE).description("Beli 1").price(10000).month(Month.JANUARY).build();
        Payment paymentTwo = Payment.builder().category(Category.EDUCATION).description("Beli 2").price(10000).month(Month.JANUARY).build();
        Payment paymentThree = Payment.builder().category(Category.EDUCATION).description("Beli 3").month(Month.JANUARY).price(10000).build();
        Payment paymentFour = Payment.builder().category(Category.ENTERTAINMENT).description("Beli 4").month(Month.JANUARY).price(10000).build();
        Payment paymentFive = Payment.builder().category(Category.SELF_CARE).description("Beli 5").month(Month.FEBRUARY).price(10000).build();
        Payment paymentSix = Payment.builder().category(Category.GOLF).description("Beli 6").month(Month.FEBRUARY).price(10000).build();
        Payment paymentSeven = Payment.builder().category(Category.EDUCATION).description("Beli 7").month(Month.FEBRUARY).price(10000).build();
        Payment paymentEight = Payment.builder().category(Category.RESTAURANT).description("Beli 8").month(Month.FEBRUARY).price(10000).build();
        Payment paymentNine = Payment.builder().category(Category.SELF_CARE).description("Beli 9").month(Month.FEBRUARY).price(10000).build();
        Payment paymentTen = Payment.builder().category(Category.GOLF).description("Beli 10").month(Month.FEBRUARY).price(10000).build();

        List<Payment> payments = Arrays.asList(paymentOne, paymentTwo, paymentThree, paymentFour, paymentFive, paymentSix, paymentSeven, paymentEight, paymentNine, paymentTen);

        PaymentRecord paymentRecord = new PaymentRecord(payments);
        SpendingAnalyzer spendingAnalyzer = new SpendingAnalyzer(paymentRecord);
        spendingAnalyzer.analyze(Month.FEBRUARY);

        System.out.println(spendingAnalyzer);
    }
}

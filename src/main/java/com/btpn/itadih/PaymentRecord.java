package com.btpn.itadih;

import java.util.*;

public record PaymentRecord(List<Payment> payments) {
    public Map<Category, Double> groupByCategory(Month month) {
        List<Payment> currentMonthPayments = this.payments.stream().filter(payment -> payment.getMonth().equals(month)).toList();

        EnumMap<Category, Double> groupedPaymentsByCategory = new EnumMap<>(Category.class);
        currentMonthPayments.forEach(payment -> {
            if (!groupedPaymentsByCategory.containsKey(payment.getCategory())) {
                groupedPaymentsByCategory.put(payment.getCategory(), (double) 0);
            }
            Double totalPrice = groupedPaymentsByCategory.get(payment.getCategory()) + payment.getPrice();
            groupedPaymentsByCategory.put(payment.getCategory(), totalPrice);
        });

        return groupedPaymentsByCategory;
    }
}

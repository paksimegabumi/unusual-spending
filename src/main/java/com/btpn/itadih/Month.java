package com.btpn.itadih;

public enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;

    private static final Month[] months = values();

    public Month previous() {
        return months[(ordinal() - 1  + months.length) % months.length];
    }
}

package com.btpn.itadih;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

@AllArgsConstructor
@Data
@Builder
public class BigSpending {
    private Category category;
    private Double totalPrice;

    @Override
    public String toString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMANY);
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        return String.format("* You spent Rp %s on %s%n", decimalFormat.format(this.totalPrice), this.category);
    }
}

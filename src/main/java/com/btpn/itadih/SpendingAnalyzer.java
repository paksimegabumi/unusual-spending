package com.btpn.itadih;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class SpendingAnalyzer {
    public static final double FIFTY_PERCENT = 0.5;
    public static final double ZERO = 0;
    private final PaymentRecord paymentRecord;
    private final List<BigSpending> bigSpending = new ArrayList<>();

    public void analyze(Month currentMonth) {
        Map<Category, Double> currentMonthPayments = this.paymentRecord.groupByCategory(currentMonth);
        Map<Category, Double> previousMonthPayments = this.paymentRecord.groupByCategory(currentMonth.previous());

        for (Map.Entry<Category, Double> currentPayment : currentMonthPayments.entrySet()) {
            Double currentMonthPaymentsByCategory = currentPayment.getValue();
            Double previousMonthPaymentsByCategory = previousMonthPayments.get(currentPayment.getKey());

            if (previousMonthPaymentsByCategory == null) {
                previousMonthPaymentsByCategory = ZERO;
            }

            addBigSpendingIfMoreThanThreshold(currentPayment, currentMonthPaymentsByCategory, previousMonthPaymentsByCategory);
        }
    }

    private void addBigSpendingIfMoreThanThreshold(Map.Entry<Category, Double> currentPayment, Double currentMonthPaymentsByCategory, Double previousMonthPaymentsByCategory) {
        Double unusualSpendingThreshold = previousMonthPaymentsByCategory + (previousMonthPaymentsByCategory * FIFTY_PERCENT);
        if (currentMonthPaymentsByCategory >= unusualSpendingThreshold) {
            BigSpending unusualSpending = BigSpending.builder().category(currentPayment.getKey()).totalPrice(currentMonthPaymentsByCategory).build();
            this.bigSpending.add(unusualSpending);
        }
    }

    @Override
    public String toString() {
        StringBuilder bigSpendingInString = new StringBuilder();
        for (BigSpending spending : this.bigSpending) {
            bigSpendingInString.append(spending.toString());
        }
        return "Hello card user!\n\nWe have detected unusually high spending on your card in these categories:\n" + bigSpendingInString + "\nLove, \nThe Credit Card Company";
    }
}

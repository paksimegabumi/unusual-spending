package com.btpn.itadih;

public enum Category {
    RESTAURANT("Restaurant"), ENTERTAINMENT("Entertainment"), EDUCATION("Education"), GOLF("Golf"), SELF_CARE("Self Care");

    private final String name;

    Category(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
